#include "framework.h"

char		g_acStatusBarProcessingInfo[ 256 ] = "" ;
HWND		g_hwndStatusBar = NULL ;
int		g_nStatusBarHeight = 0 ;

/* 留用
	char	buf[ 1024 ] ;
	int	len ;

	if( g_pnodeCurrentTabPage == NULL )
	{
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_PATHFILENAME , (LPARAM)"" );
	return ;
	}

	len = snprintf( buf , sizeof(buf)-1 , "路径文件名:%s" , g_pnodeCurrentTabPage->acPathFilename ) ;
	if( g_pnodeCurrentTabPage->pstDocTypeConfig )
	snprintf( buf+len , sizeof(buf)-1-len , " (%s)" , g_pnodeCurrentTabPage->pstDocTypeConfig->pcFileTypeDesc );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_PATHFILENAME , (LPARAM)buf );
*/

void SetProcessingInfo( const char *format , ... )
{
	va_list		valist ;

	memset( g_acStatusBarProcessingInfo , 0x00 , sizeof(g_acStatusBarProcessingInfo) );
	va_start( valist , format );
	snprintf( g_acStatusBarProcessingInfo , sizeof(g_acStatusBarProcessingInfo)-1 , format , valist );
	va_end( valist );

	UpdateStatusBarProcessingInfo();

	return;
}

void CleanProcessingInfo()
{
	memset( g_acStatusBarProcessingInfo , 0x00 , sizeof(g_acStatusBarProcessingInfo) );

	UpdateStatusBarProcessingInfo();

	return;
}

void UpdateStatusBarProcessingInfo()
{
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_PROCESSINGINFO , (LPARAM)g_acStatusBarProcessingInfo );

	return;
}

void UpdateStatusBarLocationInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
	{
		::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_LOCATIONINFO , (LPARAM)"" );
		return ;
	}

	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	int nCurrentLineNo = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;
	int nFirstPosInCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_POSITIONFROMLINE , nCurrentLineNo , 0 ) ;
	int nLastPosInCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETLINEENDPOSITION , nCurrentLineNo , 0 ) ;
	int nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
	int nMaxLineNo = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "列:%d/%d    行:%d/%d    偏移量:%d/%d"
		, nCurrentPos-nFirstPosInCurrentLine+1 , nLastPosInCurrentLine-nFirstPosInCurrentLine+1
		, nCurrentLineNo+1 , nMaxLineNo
		, nCurrentPos , nMaxPos );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_LOCATIONINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarEolModeInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
	{
		::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_EOLMODEINFO , (LPARAM)"" );
		return ;
	}

	memset( buf , 0x00 , sizeof(buf) );
	int nEolMode = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETEOLMODE , 0 , 0 ) ;
	if( nEolMode == 0 )
		strcpy( buf , "换行符模式:DOS" );
	else if( nEolMode == 1 )
		strcpy( buf , "换行符模式:MAC" );
	else if( nEolMode == 2 )
		strcpy( buf , "换行符模式:UNIX/Linux" );
	else
		strcpy( buf , "未知换行符模式" );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_EOLMODEINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarEncodingInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
	{
		::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_ENCODINGINFO , (LPARAM)"" );
		return ;
	}

	memset( buf , 0x00 , sizeof(buf) );
	if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
		strcpy( buf , "字符编码:UTF8" );
	else if( g_pnodeCurrentTabPage->nCodePage == ENCODING_GBK )
		strcpy( buf , "字符编码:GBK" );
	else if( g_pnodeCurrentTabPage->nCodePage == ENCODING_BIG5 )
		strcpy( buf , "字符编码:BIG5" );
	else
		strcpy( buf , "未知字符编码" );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_ENCODINGINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarSelectionInfo()
{
	int	nSelectionStart ;
	int	nSelectionEnd ;
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
	{
		::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_SELECTIONINFO , (LPARAM)"" );
		return ;
	}

	nSelectionStart = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
	nSelectionEnd = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;

	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "选择文本长度:%d" , nSelectionEnd-nSelectionStart );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_SELECTIONINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBar( HWND hWnd )
{
	RECT	rectClient ;
	int	anStatusBarPartRightEdge[ STATUSBAR_ITEM_COUNT ] ;

	::GetClientRect( hWnd , & rectClient );

	::SetWindowPos ( g_hwndStatusBar , HWND_TOP ,  rectClient.left , rectClient.bottom-g_nStatusBarHeight , rectClient.right-rectClient.left , rectClient.bottom , SWP_SHOWWINDOW );

	anStatusBarPartRightEdge[4] = rectClient.right ;
	anStatusBarPartRightEdge[3] = anStatusBarPartRightEdge[4] - 150 ;
	anStatusBarPartRightEdge[2] = anStatusBarPartRightEdge[3] - 150 ;
	anStatusBarPartRightEdge[1] = anStatusBarPartRightEdge[2] - 150 ;
	anStatusBarPartRightEdge[0] = anStatusBarPartRightEdge[1] - 300 ;
	::SendMessage( g_hwndStatusBar , SB_SETPARTS , STATUSBAR_ITEM_COUNT , (LPARAM)(LPINT)anStatusBarPartRightEdge );

	UpdateStatusBarProcessingInfo();
	UpdateStatusBarLocationInfo();
	UpdateStatusBarEolModeInfo();
	UpdateStatusBarEncodingInfo();
	UpdateStatusBarSelectionInfo();

	// ::ShowWindow( g_hwndStatusBar , SW_SHOW );
	::InvalidateRect( g_hwndStatusBar , NULL , TRUE );
	::UpdateWindow( g_hwndStatusBar );

	return;
}
